<?php

error_reporting(E_ALL);

include_once "config/init.php";
include_once "config/config.php";

use App\Lib\Template;
use App\Enums\Components\UrlEnums;
use App\Enums\Components\IndexEnums;

/*
    UrlEnums enums class to manage page path
    Template class to manage the content of page
*/

$template = new Template(UrlEnums::PAGES['indexpage']);

/*
    IndexEnums enums class to manage the default values
*/

$template->connectionHeader = IndexEnums::FIELDS['title_credential'];
$template->queryHeader = IndexEnums::FIELDS['title_query'];
$template->chartHeader = IndexEnums::FIELDS['chart_header'];

$template->unauthorized = '';

$template->headerChart = json_encode([]);
$template->chartTitle = "Data visualization";
$template->dataArray = [];


$rendering = $template->renderTemplate($template);

echo $rendering;
# Dynamic data visualization web application

The purpose of this web application is to display data flexibly and dynamically.

To use the application, please follow the steps below.

Start the application via the following URL http://localhost/data_visualization

A web page shows a form containing three sections:
 
DATA ACCESS allows you to provide credentials to connect to any database such as Mysql, Postgres, Sqlserver... etc. the database type uses Mysql by default, if you want to change another database type please modify, the DATABASE_TYPE variable in the config/config.php file (Mysql, Postgres, Sqlserver...etc)

The database host is used to enter the host, IP address, or server name.
The database name is used to enter the name of the database.
The user name is used to enter the name of the user who has access to the database.
The password is the user's password for accessing the database.

SQL query allows you to write a query to obtain data from a database. 
DATA VISUALIZATION allows you to view data in a graph.

3. Once you have provided all the necessary information, click on the search button.

4. If the Data Access and SQL query supplied are correct, you will see the result in the data visualization section, otherwise the application will display an error message.
If the query supplied contains different string columns, the program will delete the unused columns and format the data before sending it to the graph.
If the query supplied contains an error, such as a syntax error, or an incorrect table or column name, the program handles the error and customizes the error message.


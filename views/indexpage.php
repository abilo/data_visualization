<?php include 'shared/header.php'; ?>

<?php if(!empty($unauthorized)){ ?>
    <div class="container mx-5 justify-content-center align-items-center" style="margin:250px">
        <div class="text-center" style="margin-top:10px;padding:7px 13px">
            <p style="font-size:24px"><?php echo $unauthorized; ?></p>
        </div>      
    </div>      
<?php }else{ ?>    

<form method="POST" action="route.php">
<div class="card my-5">
    <div class="card-header mb-3">
        <h5 class="card-title">            
            <?php echo $connectionHeader; ?>
        </h5>
    </div>
    <div class="card-body px-5">
            <div class="row">
                <?php if(!empty($db_err)){ ?>
                    <div class="alert alert-danger" style="margin-top:10px;padding:7px 13px">
                        <strong>Error!</strong> <?php echo $db_err; ?>.
                    </div>            
                <?php } ?>    

                <div class="col-md-6 mb-3">
                    <input type="text" 
                           class="form-control" 
                           name="host" 
                           id="host"
                           value ="<?php if(!empty($host)){ echo $host; }?>"
                           placeholder="Database Host" required>

                    <?php if(!empty($host_err)){ ?>
                        <div class="alert alert-danger" style="margin-top:10px;padding:7px 13px">
                            <strong>Error!</strong> <?php echo $host_err; ?>.
                        </div>            
                    <?php } ?>    
                </div>
                <div class="col-md-6 mb-3">
                <input type="text" 
                           class="form-control" 
                           name="db_name" 
                           id="db_name"
                           value ="<?php if(!empty($db_name)){ echo $db_name; }?>"
                           placeholder="Database name" required>

                    <?php if(!empty($db_name_err)){ ?>
                        <div class="alert alert-danger" style="margin-top:10px;padding:7px 13px">
                            <strong>Error!</strong> <?php echo $db_name_err; ?>.
                        </div>            
                    <?php } ?>    
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                    <input type="text" 
                           class="form-control" 
                           name="user_name" 
                           id="user_name" 
                           value ="<?php if(!empty($user_name)){ echo $user_name; }?>"
                           placeholder="Db Username" required>

                    <?php if(!empty($userName_err)){ ?>
                        <div class="alert alert-danger" style="margin-top:10px;padding:7px 13px">
                            <strong>Error!</strong> <?php echo $userName_err; ?>.
                        </div>            
                    <?php } ?>    
                </div>
                <div class="col-md-6 mb-3">
                    <input type="password" 
                           class="form-control" 
                           name="pwd" 
                           id="pwd" 
                           value ="<?php if(!empty($pwd)){ echo $pwd; }?>"
                           placeholder="Db Password">

                    <?php if(!empty($pwd_err)){ ?>
                        <div class="alert alert-danger" style="margin-top:10px;padding:7px 13px">
                            <strong>Error!</strong> <?php echo $pwd_err; ?>.
                        </div>            
                    <?php } ?>    
                </div>
            </div>
    </div>
</div>

<div class="card mb-5">
    <div class="card-header mb-3">
        <h5 class="card-title">            
            <?php echo $queryHeader; ?>
        </h5>
    </div>
    <div class="card-body px-5">
        <div class="row">
            <div class="col-md-12 mb-3">
                <textarea class="form-control" placeholder="Leave a query here" id="q" name="q" rows="5" required><?php if(!empty($q)){ echo $q; }?></textarea>
                <?php if(!empty($q_err)){ ?>
                    <div class="alert alert-danger" style="margin-top:10px;padding:7px 13px">
                        <strong>Error!</strong> <?php echo $q_err; ?>.
                    </div>            
                <?php } ?>    
            </div>
        </div>

        <div class="row">    
            <div class="col-md-12 mt-3 mb-1">
                <button type="submit" name="query" class="btn btn-primary">Search</button>
            </div>
        </div>
    </div>

</div>
</form>

<div class="card mb-5">
    <div class="card-header mb-3">
        <h5 class="card-title">            
            <?php echo $chartHeader; ?>
        </h5>
    </div>
    <div class="card-body px-0">
        <div class="row">
            <div id="curve_chart"></div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
    var chartTitle = "<?php echo $chartTitle; ?>";

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
  

    function drawChart() {
    var data = google.visualization.arrayToDataTable([
       <?php echo json_encode($headerChart); ?>,
       <?php foreach($dataArray as $data) {?>
       <?php echo json_encode($data); ?>,
       <?php }?>
    ]);

    var options = {
        title: chartTitle,
        curveType: 'function',
        legend: { position: 'bottom' }
    };

    var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
//    var chart = new google.visualization.PieChart(document.getElementById('curve_chart'));
    chart.draw(data, options);

    }

</script>
<?php } ?>    

<?php include 'shared/footer.php'; ?>

<?php

namespace App\Enums\Components;

final class IndexEnums 
{

/*
    ASSIGN THE DEFAULT VALUES 
*/

    const FIELDS = [
        'title_credential' => 'DATA ACCESS',
        'title_query' => 'SQL QUERY',
        'chart_header' => 'DATA VISUALIZATION',
        'unauthorized'=> 'Error Unauthorized Access'
    ];

}

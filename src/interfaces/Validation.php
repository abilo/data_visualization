<?php

namespace App\Interfaces;

interface Validation
{
/*
    VALIDATION INTERFACE 
*/

    public function rules():array;
    public function messages():array;
}

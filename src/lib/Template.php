<?php 

namespace App\Lib;

class Template
{

/*
    Template CLASS ITS A WAY TO MANAGE THE CONTENT OF THE PAGE BEFORE SHOWING TO THE USER   
*/

    private $template;
    private $props =[];

    public function __construct($template)
    {
        $this->template = $template;
    }

/*
    __get FUNCTION TO MANAGE NEW CONTENT   
*/
    public function __get($key)
    {
        return $this->props[$key];
    }

/*
    __set FUNCTION TO SET NEW CONTENT   
*/
    public function __set($key, $value)
    {
        $this->props[$key] = $value;
    }

/*
    renderTemplate FUNCTION RENDERING A NEW CONTENT   
*/

    public function renderTemplate()
    {
        ob_start();
        extract($this->props);

        include $this->template;

        return ob_get_clean();
    }
    
}
<?php

namespace App\Lib\Controllers;

use App\helpers\ValidationConnection;
use App\Lib\Services\DatabaseService;
use App\Lib\Services\ChartDataService;

class ConnectController 
{

/*
    MANAGE THE DATA REQUEST BEFORE SEND IT TO DATABASE/ FORMATTING DATA BEFORE SEND TO VIEW 
*/


/*
    checkRequest FUNCTION TO MANAGE THE DATA SEND IT BY USER
*/
    public static function checkRequest($request, $template)
    {
        $data =[];

        $data['host'] = $request['host'];
        $data['db_name'] = $request['db_name'];
        $data['user_name'] = $request['user_name'];
        $data['pwd'] = $request['pwd'];

        $data['q'] = $request['q'];

        $template->result = $data;            
        $template->parameters = [];            

        $validation = new ValidationConnection($data);
        $validator = $validation->rules();

        if(count($validator)>0)
        {
            if(array_key_exists('host', $validator) !== false) $template->host_err = $validator['host'];
            if(array_key_exists('db_name', $validator) !== false) $template->db_name_err = $validator['db_name'];
            if(array_key_exists('user_name', $validator) !== false) $template->userName_err = $validator['user_name'];
            if(array_key_exists('pwd', $validator) !== false) $template->pwd_err = $validator['pwd'];

            if(array_key_exists('q', $validator) !== false) $template->q_err = $validator['q'];
            
            $response = [
                'error' => true,
                'template' => $template,
                'data'=> $data
            ];

            return $response;
        }

        $response = [
            'error' => false,
            'template' => $template,
            'data'=> $data
        ];

        return $response;
    }

/*
    setRequest FUNCTION TO MANAGE THE CREDENTIALS INPUTS
*/
    public static function setRequest($template, $data)
    {        
        $dataService = new DatabaseService();
        $dataServiceError = $dataService->connection($data);

        if($dataServiceError['error'] == true)
        {
            $template->db_err = $dataServiceError['message'];

            return [
                'error' => true,
                'template' => $template,
                'data'=> $data
            ];    
        }

        $resultSetQuery = self::setQuery($template, $dataService, $data);

        return [
            'error' => $resultSetQuery['error'],
            'template' => $resultSetQuery['template'],
            'data'=> $resultSetQuery['data']
        ];
    }

/*
    setQuery FUNCTION TO MANAGE THE QUERY INPUTS
*/
    public static function setQuery($template, $dataService, $data)
    {
        if(!empty($data['q']) && !is_null($data['q']))
        {
            $dataResult = $dataService->getData($data['q']);

            if($dataResult['error'] == true)
            {
                $template->db_err = $dataResult['message'];
    
                return [
                    'error' => true,
                    'template' => $template,
                    'data'=> $data
                ];    
            }

            $parameters = $dataService->getQueryParams($dataResult['parameter']);

            $data['result'] = $dataResult['data'];            
            $data['parameters'] = $parameters;            

            return [
                'error' => false,
                'template' => $template,
                'data'=> $data
            ];    
        }else{
            $template->db_err = 'Query is empty';

            return [
                'error' => true,
                'template' => $template,
                'data'=> $data
            ];    
        }
    }
/*
    formatResponse FUNCTION FORMATTING RESPONSE BEFORE REPLY TO THE USER
*/
    public static function formatResponse($template, $data=null)
    {
        if(!is_null($data))
        {
            $template->host = $data['host'];
            $template->db_name = $data['db_name'];
            $template->user_name = $data['user_name'];
            $template->pwd = $data['pwd'];
            $template->q = $data['q'];    
        }
        
        return $template->renderTemplate($template);
    }

/*
    formatColumns FUNCTION MANAGE AND FORMATTING COLUMNS BEFORE SEND IT TO THE CHART
*/
    public static function formatColumns($template, $data)
    {
        if(count($data) >0)
        {
            $chartDataService = new ChartDataService($data);
            $checkResult = $chartDataService->checkUnusedCol('string');

            if($checkResult['status'] == true)
                $checkResult['array'] = $chartDataService->removeUnusedKey($checkResult['array'], $checkResult['columns']);    

            $checkResult['array'] = $chartDataService->formatChartData($checkResult['array']);    

            $template->headerChart = $checkResult['array']['headerChart'];
            $template->dataArray = $checkResult['array']['bodyChart']; 
            $template->chartTitle = $checkResult['chartTitle'];
    
        }else{
            $template->headerChart = [];
            $template->dataArray = [];
            $template->chartTitle = '';
        }    

        return [
            'template' => $template,
            'data'=> $data
        ];
    }
}
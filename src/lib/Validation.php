<?php

namespace App\Lib;

// use App\Interfaces\Validation;

class Validation // implements Validation
{

    private $data = [];
//    private bool $is_new = true;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function rules():array
    {
        $errors =[];

        if(empty($this->data['host'])) $errors['host'] = "host is required";
        if(empty($this->data['typeDb'])) $errors['typeDb'] = "Type database is required";
        if(empty($this->data['userName'])) $errors['userName'] = "User name is required";
        if(empty($this->data['pwd'])) $errors['pwd'] = "password is required";

        return $errors;
    }

    public function messages():array
    {
        return [];
    }

}

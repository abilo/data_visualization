<?php

namespace App\Lib\Services;

include_once "config/config.php";

use App\Helpers\Helper;

class ChartDataService 
{

/*
    ChartDataService CLASS TO MANAGE CHART DATA AND REMOVE UNUSED COLUMNS
*/
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

/*
    checkUnusedCol FUNCTION CHECK AVAILABILITY UNUSED COLUMNS
*/
    public function checkUnusedCol($unusedDataType) :array
    {
        $data = $this->data;
        $unusedCol =[];

        if(count($data['result'])>0)
        {    
            $result = $data['result'];

            $chartTitle = $data['parameters']['table'];
            $resultGrouped = $data['parameters']['grouped'];
    
            $dataType =[];
            
            if(is_array($result[0]))
            {
                foreach ($result[0] as $key => $value) 
                {      
                    Helper::filterValue($value);
    
                    $dataType[$key] = gettype($value);
                }    
            }
    
            $temp_array = array_unique($dataType);
            $doublicatString = count($temp_array) != count($dataType);
                        
            if($doublicatString == true)
            {    
                foreach ($dataType as $key => $value) 
                {
                    if($value == $unusedDataType && $key != $resultGrouped)
                    {
                        $unusedCol[$key] = $key;
                    }
                }        
            }
        }

        return [
            'status' => true,
            'chartTitle' => $chartTitle,
            'columns' => $unusedCol,
            'array' => $result
        ];
    }


/*
    removeUnusedKey FUNCTION REMOVE UNUSED COLUMNS
*/
    public function removeUnusedKey(array &$array, array $keysToRemove) :array
    {
        foreach ($array as $key => &$value) 
        {
            if (is_array($value)) 
            {
                $this->removeUnusedKey($value, $keysToRemove);
            } else {
                if (in_array($key, $keysToRemove)) 
                {
                    unset($array[$key]);
                }
            }
        }

        return $array;
    }

/*
    formatChartData FUNCTION FORMAT CHART DATA
*/

    public function formatChartData($array)
    {
        $headerChart =[];
        $dataList =[];

        foreach ($array as $row) 
        {
            if(!is_array($row)) continue;
    
            if(count($headerChart)==0) $headerChart = array_keys($row);
    
            $lineArray = [];
            
            foreach ($row as $col) 
            {
                Helper::filterValue($col);

                array_push($lineArray, $col);
            }
            array_push($dataList, $lineArray);
        }


        return [
            'headerChart' => $headerChart,
            'bodyChart' => array_values($dataList)            
        ];
    }

}


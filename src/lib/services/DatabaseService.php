<?php

namespace App\Lib\Services;

include_once "config/config.php";
use App\Lib\Database\Mysql\MysqlDatabase;

class DatabaseService 
{

/*
    DatabaseService CLASS MANAGE THE CONNECTION AND QUERIES BEFORE SEND IT TO DATABASE CLASS
*/

    private $db;
    private $databaseType;
    private $result;

    public function __construct()
    {
        $this->databaseType = "App\\Lib\\Database\\".DATABASE_TYPE."\\".DATABASE_TYPE."Database";
    }

/*
    connection FUNCTION MANAGE THE CONNECTION TO DATABASE CLASS
*/

    public function connection($request)
    {
        if(count($request)>0)
        {
            $host = $_POST['host'];
            $db_name = $_POST['db_name'];
            $user = $_POST['user_name'];
            $pwd = $_POST['pwd'];
    
            $this->db = new $this->databaseType($host, $db_name, $user, $pwd);
            $this->db->connect();;

            $errorInfo = $this->db->getError();

            if(!is_null($errorInfo)) 
            {
                return [
                    'error' => true,
                    'message' => $errorInfo
                ];
            }
        }

        return [
            'error' => false,
            'message' => $errorInfo
        ];
    }

/*
    getError FUNCTION MANAGE THE UNEXPECTED ERRORS
*/

    public function getError()
    {
        return $this->db->error;
    }

/*
    getData FUNCTION GET QUERY RESULT
*/
    public function getData($query)
    {
        $this->db->query($query);

        return $this->db->resultSet();
    }

/*
    getQueryParams FUNCTION GET QUERY PARAMETERS
*/
    public function getQueryParams($parameter)
    {
        $groupedColumn = $parameter->getColumnMeta(0);

        $response = [
            'table' => $groupedColumn['table'],
            'grouped' => $groupedColumn['name']
        ];

        return $response;

    }

}


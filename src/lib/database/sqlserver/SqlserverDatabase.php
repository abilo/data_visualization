<?php

namespace App\Lib\Database\Sqlserver;

use App\Lib\Database\Database;
use PDO;
use PDOException;

class SqlserverDatabase extends Database 
{

/*
    SqlserverDatabase CLASS TO HANDLE CONNECTION AND QUERIES TO THE SQL SERVER DATABASE
*/

    private $host;
    private $user;
    private $pwd;
    private $db_name;

    private $dbh;

    private $error;
    private $stmt;

    public function __construct($host, $db_name, $user, $pwd)
    {    
        $this->host = $host;
        $this->user = $user;
        $this->pwd = $pwd;
        $this->db_name = $db_name;
    
    }

    public function connect() 
    {
        $dsn = "sqlsrv:Server=".$this->host.";Database=".$this->db_name;

        $options=[
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];

        try{
            $this->dbh = new PDO($dsn, $this->user, $this->pwd, $options);            
        }catch(PDOException $e){
            $this->error = $e->getMessage();
        }
    }

    public function getError() 
    {        
        return $this->error;
    }

    public function query($query) 
    {        
        $this->stmt =  $this->dbh->prepare($query);
    }

    public function execute()
    {
        return $this->stmt->execute();
    }

    public function resultSet()
    {
        $this->execute();
        $result = $this->stmt->fetchAll(PDO::FETCH_ASSOC);

        return [
            'data' => $result,
            'parameter' => $this->stmt
        ];
    }

    public function close() {
    }
}

?>

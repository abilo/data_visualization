<?php

namespace App\Lib\Database\Mysql;

use App\Lib\Database\Database;
use PDO;
use PDOException;

class MysqlDatabase extends Database 
{

/*
    MysqlDatabase CLASS TO HANDLE CONNECTION AND QUERIES TO THE MYSQL DATABASE
*/

    private $host;
    private $user;
    private $pwd;
    private $db_name;

    private $dbh;
    private $error;
    private $stmt;

    public function __construct($host, $db_name, $user, $pwd)
    {    
        $this->host = $host;
        $this->user = $user;
        $this->pwd = $pwd;
        $this->db_name = $db_name;
    
    }

    public function connect() 
    {
        $dsn = "mysql:host=".$this->host.";dbname=".$this->db_name;

        $options=[
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];

        try{
            $this->dbh = new PDO($dsn, $this->user, $this->pwd, $options);            
        }catch(PDOException $e){
            $this->error = $e->getMessage();
        }
    }

    public function getError() 
    {        
        return $this->error;
    }

    public function query($query) 
    {        
        $this->stmt =  $this->dbh->prepare($query);
    }

    public function execute()
    {
        try {
            return $this->stmt->execute();
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
        }        
    }

    public function resultSet()
    {
        $this->execute();
        if(!is_null($this->error))
        {
            return [
                'error' => true,
                'message' => $this->error
            ];    
        }

        $result = $this->stmt->fetchAll(PDO::FETCH_ASSOC);

        return [
            'error' => false,
            'data' => $result,
            'parameter' => $this->stmt
        ];
    }

    public function close() {
    }
}

?>

<?php

namespace App\Lib\Database;

abstract class Database 
{

/*
    Database ABSTRACT CLASS
*/
    
    private $dbh;
    private $error;
    private $stmt;

    abstract public function connect();
    abstract public function query($query);
    abstract public function execute();
    abstract public function resultSet();

    abstract public function close();



}

?>

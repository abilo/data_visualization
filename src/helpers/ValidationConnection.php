<?php

namespace App\helpers;

use App\Interfaces\Validation;

class ValidationConnection  implements Validation
{

/*
    VALIDATE DATA REQUEST 
*/

    private $data = [];

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function rules():array
    {
        $errors =[];

        if(empty($this->data['host'])) $errors['host'] = "host is required";
        if(empty($this->data['db_name'])) $errors['db_name'] = "Type database is required";
        if(empty($this->data['user_name'])) $errors['user_name'] = "User name is required";
//        if(empty($this->data['pwd'])) $errors['pwd'] = "password is required";

        return $errors;
    }

    public function messages():array
    {
        return [];
    }

}

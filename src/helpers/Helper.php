<?php

namespace App\helpers;

class Helper
{

/*
    HELPER TO CONVERT AND VALIDATE DATA TYPE  
*/

    public static function filterValue(&$value)
    {
        if (filter_var($value, FILTER_VALIDATE_INT)) $value = (int) $value;
        if (filter_var($value, FILTER_VALIDATE_FLOAT)) $value = (float) $value;
    }

}

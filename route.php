<?php

namespace App;

include_once "config/init.php";

use App\Lib\Controllers\ConnectController;
use App\Lib\Controllers\QueryController;
use App\Lib\Template;
use App\Enums\Components\IndexEnums;
use App\Enums\Components\UrlEnums;

$template = new Template(UrlEnums::PAGES['indexpage']);

$template->connectionHeader = IndexEnums::FIELDS['title_credential'];
$template->queryHeader = IndexEnums::FIELDS['title_query'];
$template->chartHeader = IndexEnums::FIELDS['chart_header'];


if(isset($_POST['query']))
{
    $request = $_POST;
    $dataResult =  ConnectController::checkRequest($request, $template);
    
    if($dataResult['error'] == false)
    {
        $dataResult = ConnectController::setRequest($dataResult['template'], $dataResult['data']);

        if($dataResult['error'] == false)
            $dataResult = ConnectController::formatColumns($dataResult['template'], $dataResult['data']);    
    }  
//            header("Location: index.php");

    $dataResult = ConnectController::formatResponse($dataResult['template'], $dataResult['data']);    
 
   echo $dataResult;
}else{
    $template->unauthorized = IndexEnums::FIELDS['unauthorized'];
    $unauthorizedResult = ConnectController::formatResponse($template);    

    echo $unauthorizedResult;
}
